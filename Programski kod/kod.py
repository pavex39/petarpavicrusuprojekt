import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
import streamlit as st
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error

# Load the Boston dataset
boston_df = pd.read_csv('boston_house_prices.csv')

# Load the California dataset
california_df = pd.read_csv('housing.csv')
california_df.dropna(inplace=True)
california_df = california_df.drop('ocean_proximity', axis=1)

# Split features and target variable for Boston dataset
X_boston = boston_df.drop('MEDV', axis=1)
y_boston = boston_df['MEDV']

# Split features and target variable for California dataset
X_california = california_df.drop('median_house_value', axis=1)
y_california = california_df['median_house_value']

# Split the data into training, testing and validating sets for Boston dataset
X_train_boston, X_test_boston, y_train_boston, y_test_boston = train_test_split(X_boston, y_boston, test_size=0.2)
X_train_boston, X_valid_boston, y_train_boston, y_valid_boston = train_test_split(X_boston, y_boston, test_size=0.1)

# Split the data into training, testing and validating sets for California dataset
X_train_california, X_test_california, y_train_california, y_test_california = train_test_split(X_california, y_california, test_size=0.2)
X_train_california, X_valid_california, y_train_california, y_valid_california = train_test_split(X_california, y_california, test_size=0.1)

# Preprocessing
scaler = StandardScaler()
preprocessor_boston = ColumnTransformer([('num_transformer', scaler, X_boston.columns)], remainder='passthrough')
preprocessor_california = ColumnTransformer([('num_transformer', scaler, X_california.columns)], remainder='passthrough')
X_train_boston = preprocessor_boston.fit_transform(X_train_boston)
X_test_boston = preprocessor_boston.transform(X_test_boston)
X_train_california = preprocessor_california.fit_transform(X_train_california)
X_test_california = preprocessor_california.transform(X_test_california)

# Streamlit UI
def main():
    st.title("House Price Prediction")
    st.write("Please select a dataset and enter the information about the house")

    # Dataset selection
    dataset_choice = st.selectbox("Select a dataset:", ['Boston', 'California'])

    # User input features
    if dataset_choice == 'Boston':
        CRIM = st.number_input("CRIM (per capita crime rate):", min_value=0.01, max_value=100.0, step=0.10)
        ZN = st.number_input("ZN (proportion of residential land zoned for lots over 25,000 sq.ft.):", min_value=0.0, max_value=100.0, step=1.0)
        INDUS = st.number_input("INDUS (proportion of non-retail business acres per town):", min_value=0.3, max_value=30.0, step=0.5)
        CHAS = st.selectbox("CHAS (Charles River dummy variable):", [0, 1])
        NOX = st.number_input("NOX (nitric oxides concentration):", min_value=0.35, max_value=0.90, step=0.01)
        RM = st.number_input("RM (average number of rooms per dwelling):", min_value=3.5, max_value=9.0, step=0.25)
        AGE = st.number_input("AGE (proportion of owner-occupied units built prior to 1940):", min_value=2.0, max_value=120.0, step=1.0)
        DIS = st.number_input("DIS (weighted distances to five Boston employment centers):", min_value=1.0, max_value=15.0, step=0.25)
        RAD = st.number_input("RAD (index of accessibility to radial highways):", min_value=1.0, max_value=24.0, step=0.5)
        TAX = st.number_input("TAX (full-value property-tax rate per $10,000):", min_value=150.0, max_value=750.0, step=5.0)
        PTRATIO = st.number_input("PTRATIO (pupil-teacher ratio by town):", min_value=10.0, max_value=25.0, step=1.0)
        B = st.number_input("B (1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town):", min_value=0.2, max_value=400.0, step=0.25)
        LSTAT = st.number_input("LSTAT (percentage lower status of the population):", min_value=1.5, max_value=40.0, step=0.25)
        
        X_test = X_test_boston
        y_test = y_test_boston

        user_input = pd.DataFrame(
            [[CRIM, ZN, INDUS, CHAS, NOX, RM, AGE, DIS, RAD, TAX, PTRATIO, B, LSTAT]],
            columns=X_boston.columns
        )

        user_input = preprocessor_boston.transform(user_input)
        X_train = X_train_boston
        y_train = y_train_boston
        
        X_valid = X_valid_boston
        y_valid = y_valid_boston
        
    else:
        # User input features for California dataset
        longitude = st.number_input("Longitude:", min_value=-125.0, max_value=-113.0, step=0.2)
        latitude = st.number_input("Latitude:", min_value=32.0, max_value=42.0, step=0.1)
        housing_median_age = st.number_input("Housing Median Age:", min_value=1.0, max_value=52.0, step=0.5)
        total_rooms = st.number_input("Total Rooms:", min_value=2.0, max_value=40000.0, step=1.0)
        total_bedrooms = st.number_input("Total Bedrooms:", min_value=1.0, max_value=6500.0, step=1.0)
        population = st.number_input("Population:", min_value=3.0, max_value=37000.0, step=100.0)
        households = st.number_input("Households:", min_value=1.0, max_value=6100.0, step=1.0)
        median_income = st.number_input("Median Income:", min_value=0.50, max_value=15.0, step=0.05)
        
        X_test = X_test_california
        y_test = y_test_california

        user_input = pd.DataFrame(
            [[longitude, latitude, housing_median_age, total_rooms, total_bedrooms, population,
              households, median_income]],
            columns=X_california.columns
        )

        user_input = preprocessor_california.transform(user_input)
        X_train = X_train_california
        y_train = y_train_california
        
        X_valid = X_valid_california
        y_valid = y_valid_california

    # Choose the model for prediction
    model_choice = st.selectbox("Select a model for prediction:", ['Linear Regression', 'KNN', 'SVM', 'Decision Tree'])

    if model_choice == 'Linear Regression':
        lr_model = LinearRegression()
        lr_model.fit(X_train, y_train)
        pred_model = lr_model
        
        y_pred_lr = pred_model.predict(X_test)
        mse_lr = mean_squared_error(y_test, y_pred_lr)
        mae_lr = mean_absolute_error(y_test, y_pred_lr)
        st.subheader("Linear regression preformance")
        st.write("MSE:", mse_lr)
        st.write("MAE:", mae_lr)
        
    elif model_choice == 'KNN':
        knn = KNeighborsRegressor()
        param_grid = {'n_neighbors': [3, 5, 7, 9]}
        grid_search = GridSearchCV(knn, param_grid, cv=5)
        grid_search.fit(X_valid, y_valid)
        knn_model = KNeighborsRegressor(n_neighbors=grid_search.best_params_['n_neighbors'])
        knn_model.fit(X_train, y_train)
        pred_model = knn_model
        
        y_pred_knn = pred_model.predict(X_test)
        mse_knn = mean_squared_error(y_test, y_pred_knn)
        mae_knn = mean_absolute_error(y_test, y_pred_knn)
        st.subheader("KNN performance")
        st.write("MSE:", mse_knn)
        st.write("MAE:", mae_knn)
        
    elif model_choice == 'SVM':
        param_grid = {'C': [0.1, 1, 10], 'gamma': [0.1, 0.01, 0.001]}
        grid_search = GridSearchCV(SVR(), param_grid, cv=5)
        grid_search.fit(X_valid, y_valid)
        svm_model = SVR(C=grid_search.best_params_['C'], gamma=grid_search.best_params_['gamma'])
        svm_model.fit(X_train, y_train)
        pred_model = svm_model
        
        y_pred_svm = pred_model.predict(X_test)
        mse_svm = mean_squared_error(y_test, y_pred_svm)
        mae_svm = mean_absolute_error(y_test, y_pred_svm)
        st.subheader("SVM performance")
        st.write("MSE:", mse_svm)
        st.write("MAE:", mae_svm)
        
    else:
        dt_model = DecisionTreeRegressor()
        param_grid = {'max_depth': [3, 5, 7], 'min_samples_split': [2, 4, 6]}
        grid_search = GridSearchCV(dt_model, param_grid, cv=5)
        grid_search.fit(X_valid, y_valid)
        dt_model = DecisionTreeRegressor(max_depth=grid_search.best_params_['max_depth'], min_samples_split=grid_search.best_params_['min_samples_split'])
        dt_model.fit(X_train, y_train)
        pred_model = dt_model
        
        y_pred_dt = pred_model.predict(X_test)
        mse_dt = mean_squared_error(y_test, y_pred_dt)
        mae_dt = mean_absolute_error(y_test, y_pred_dt)
        st.subheader("Decision tree performance")
        st.write("MSE", mse_dt)
        st.write("MAE:", mae_dt)

    # Predictions
    pred = pred_model.predict(user_input)

    st.subheader("Predicted House Price")
    st.write(model_choice + ":", pred[0])
    
if __name__ == '__main__':
    main()